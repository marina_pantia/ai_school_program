/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package texniti1;

/**
 *
 * @author Kostas
 */
public class Professor {
    private String teacherName;
    private String teacherId; 
    private String lessonId;
    private int maxDailyHour;
    private int maxWeaklyHour;

    public Professor(String teacherName, String teacherId, String lessonId, int maxDailyHour, int maxWeaklyHour) {
        this.teacherName = teacherName;
        this.teacherId = teacherId;
        this.lessonId = lessonId;
        this.maxDailyHour = maxDailyHour;
        this.maxWeaklyHour = maxWeaklyHour;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public String getLessonId() {
        return lessonId;
    }

    public int getMaxDailyHour() {
        return maxDailyHour;
    }

    public int getMaxWeaklyHour() {
        return maxWeaklyHour;
    }
    
    
}
