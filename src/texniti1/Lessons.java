/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package texniti1;

import java.util.*;

/**
 *
 * @author Kostas
 */
public class Lessons {
   private String lessonId;
   private String lessonName;
   private int[] HoursPerClass = new int [3];
   private ArrayList<Professor> professorsThatCanTeachThis;

    public Lessons(String lessonId, String lessonName, int HourClA, int HourClB, int HourClC) {
        this.lessonId = lessonId;
        this.lessonName = lessonName;
        this.HoursPerClass[0] = HourClA;
        this.HoursPerClass[1] = HourClB;
        this.HoursPerClass[2] = HourClC;
        professorsThatCanTeachThis = new ArrayList<>();
    }

    public String getLessonId() {
        return lessonId;
    }

    public String getLessonName() {
        return lessonName;
    }

    public int[] getHoursPerClass() {
        return HoursPerClass;
    }

    public ArrayList<Professor> getProfessors() {
        return professorsThatCanTeachThis;
    }
    
    public void addProfessor (Professor professor){
        professorsThatCanTeachThis.add(professor);
    }

}
