/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package texniti1;

import java.util.*;

/**
 *
 * @author Kostas
 */
public class Node implements Comparable <Node>{
    String [][] program;
    private int fitness;
    int [][] remDHours;
    int [] remWeekHours;
    int [] weekHours;
    ArrayList <Professor> Professors;
    ArrayList <Lessons> Lessons;
    int [][] teacherprogram;

    public Node(String[][] program, int[][] remDHours, int[] remWeekHours, ArrayList<Professor> Professors, ArrayList<Lessons> Lessons) {
        this.program = program;
        this.remDHours = remDHours;
        this.remWeekHours = remWeekHours;
        this.weekHours = new int [Professors.size()];
        this.Professors = Professors;
        this.Lessons = Lessons;
        fitness = calculateFitness();
        teacherprogram = new int [Professors.size()][35];
    }

    public Node(Node temp) {
        program = new String [9][35];
        for (int i=0;i<9;i++){
            for (int j=0;j<35;j++){
                program[i][j] = temp.program[i][j];
            }
        }
        this.remDHours = temp.remDHours.clone();
        this.remWeekHours = temp.remWeekHours.clone();
        this.weekHours = temp.weekHours.clone();
        this.Professors = (ArrayList<Professor>) temp.Professors.clone();
        this.Lessons = (ArrayList<Lessons>) temp.Lessons.clone();
        this.teacherprogram = new int [Professors.size()][35];
        for (int i=0;i<Professors.size();i++){
            for (int j=0;j<35;j++){
                this.teacherprogram[i][j] = temp.teacherprogram[i][j];
            }
        }
            
        this.teacherprogram = temp.teacherprogram.clone();
    }

    

    public String[][] getProgram() {
        return program.clone();
    }

    public int getFitness() {
        return fitness;
    }
    
    public int calculateFitness(){
        int sum = 0;
        teacherprogram = new int [Professors.size()][35];
        int [][] maxdailyhourperlesson = new int [Lessons.size()][3];
        for (int i = 0;i<Lessons.size();i++){
            for (int j =0;j<3;j++){
                int weekH = Lessons.get(i).getHoursPerClass()[j];
                if (weekH<5){
                    maxdailyhourperlesson[i][j] = 2;
                }
                else if (weekH<8){
                    maxdailyhourperlesson[i][j] = 3;
                }
                else if (weekH<14){
                    maxdailyhourperlesson[i][j] = 4;
                }
                else {
                    maxdailyhourperlesson[i][j] = 5;
                }
            }
        }
        
        for (int i=0;i<9;i++){
            int [][] lessonperday = new int [Lessons.size()][5];
            int [] dailyhour = new int [5];
            for (int k =0;k<5;k++) dailyhour[k]=0;
            for(int j=0;j<35;j++){
                if (!program[i][j].equals("KENO")) {
                    dailyhour[j/7]++;
                    for (int k=0;k<Professors.size();k++){
                        if (program[i][j].startsWith(Professors.get(k).getTeacherId())){
                            if (teacherprogram[k][j]!=0) sum += 10000000;
                            teacherprogram[k][j] = i+1;    // epeidi exei arkikopoiithei me 0 o pinakas apothikeuoume tin taxi pou didaskei 3ekinontas apo to 1
                            break;
                        } 
                    }
                    for (int k=0;k<Lessons.size();k++){
                        if (program[i][j].endsWith(Lessons.get(k).getLessonId())){
                            lessonperday[k][j/7]++;
                            break;
                        }
                    }
                }
                if ( (program[i][j].equals("KENO") ) && (j%7)!=0 && (j&7)!=6){
                    switch (j%7) {
                        case 1:
                            if (!program[i][j-1].equals("KENO")) sum+=100;
                            break;
                        case 2:
                            if (!program[i][j-1].equals("KENO")&&!program[i][j-2].equals("KENO")) sum+=100;
                            break;
                        case 4:
                            if (!program[i][j+1].equals("KENO")&&!program[i][j+2].equals("KENO")) sum+=100;
                            break;
                        case 5:
                            if (!program[i][j+1].equals("KENO")) sum+=100;
                            break;
                        default:
                            sum+=100;
                            break;
                    }
                }
                int min = 8;
                int max = -1;
                for (int k=0;k<5;k++){
                    if (dailyhour[k]<min) min =dailyhour[k];
                    if (dailyhour[k]>max) max =dailyhour[k];
                }
                if ( (max - min) > 2) sum += 20;
            }
            for (int k=0;k<Lessons.size();k++){
                for (int l=0;l<5;l++){
                    if (lessonperday[k][l]>maxdailyhourperlesson[k][i/3]) sum+=100;
                }
            } 
        }
        for (int i=0;i<Professors.size();i++){
            weekHours[i] = 0;
            int sinexomenesores =0;
            int imerOresDidas = 0;
            for (int j = 0;j<35;j++){
                if (j%7==0) {
                    sinexomenesores = 0;
                    if (imerOresDidas > Professors.get(i).getMaxDailyHour() ) sum += 1000000;
                    imerOresDidas = 0;
                }
                if (teacherprogram[i][j] != 0){
                    weekHours[i]++;
                    sinexomenesores++;
                    if (sinexomenesores > 2) sum += 50;
                }
                else {
                    sinexomenesores = 0;
                }
            }
        }
        for (int i=0;i<Professors.size();i++){
            if (weekHours[i] > Professors.get(i).getMaxWeaklyHour()) sum += 1000000;
        }
        return sum;
    }
    
    public Node mutate (Node parent, int chance){
        Node child = new Node(parent);
        Random rand = new Random();
        for (int i=0;i<9;i++){
            boolean change;
            if (rand.nextInt(100)<chance){
                do{
                    change = false;
                    int first = rand.nextInt(35);
                    int second;
                    do{ second = rand.nextInt(35); }while (first!=second);
                    int firstTeacher =-1;
                    int secondTeacher=-1;
                    for (int k=0;k<child.Professors.size();k++){
                        if (child.program[i][first].startsWith(child.Professors.get(k).getTeacherId())){
                            firstTeacher = k;
                        }
                        if (child.program[i][second].startsWith(child.Professors.get(k).getTeacherId())){
                            secondTeacher = k;
                        }
                    }
                    if ( (child.program[i][first].equals("KENO") || child.teacherprogram[firstTeacher][second]==0 ) && (child.program[i][second].equals("KENO") || child.teacherprogram[secondTeacher][first]==0 ) ){
                        if (firstTeacher!=-1){
                            child.teacherprogram[firstTeacher][first]=0;
                            child.teacherprogram[firstTeacher][second]= i+1;
                        }
                        if (secondTeacher!= -1){
                            child.teacherprogram[secondTeacher][second]=0;
                            child.teacherprogram[secondTeacher][first]= i+1;
                        }
                        String temp = child.program[i][first];
                        child.program[i][first] = child.program[i][second];
                        child.program[i][second] = temp;
                        change = true;
                        System.out.println("I changed the program");
                    }
                }while (!change);
            }
        }
         for(int i=0; i<child.program.length;i++){
            for(int j=0; j<child.program[0].length;j++){
               System.out.print(child.program[i][j] + " ");
            }
         }
         System.out.println();
        child.fitness = child.calculateFitness();
        return child;
    }
    
    @Override
    public int compareTo(Node node) {
        return ((Comparable)this.fitness).compareTo(node.fitness); //epistrefei > 0 an likes > song.likes
    }
}
