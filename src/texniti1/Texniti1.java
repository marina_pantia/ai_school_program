/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package texniti1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


/**
 *
 * @author Kostas
 */
public class Texniti1 {
    /**
     * @param args the command line arguments
     */
    @SuppressWarnings("empty-statement")
    public static void main(String[] args) {
        ArrayList <Node> Nodes = new ArrayList <>();
        int check;
        boolean goodProgram = true;
        boolean added;
        List<String> teachercode;
        String[] a = {"GAN","ABA","BGE","XDH","FEL","AIO","LPE","PPA","IPA","APA","GZA","PHL","BTH","XKO","FME","MTO","ALA","MNI","IXA","LPA"};
        teachercode = Arrays.asList(a);
        String [] b = {"GL","AR","BI","XI","FI","AR","LO","PL","IS","AG","GL","PL","BI","XI","FI","MA","AG","MA","IS","LO"};
        List <String> TeachLesson  =  Arrays.asList(b);
        Integer[] c = {5,4,4,4,4,4,4,4,4,3,5,4,4,4,4,5,3,5,4,4};
        List <Integer> maxdaily = Arrays.asList(c);
        Integer d[] = {20,18,18,18,18,18,18,18,18,12,20,18,18,18,18,20,12,20,18,18};
        ArrayList<Integer> maxweek = new ArrayList<>();
        for (int i=0;i<d.length;i++) maxweek.add(d[i]);
        String[] e = {"GL","MA","BI","XI","FI","AR","LO","PL","IS","AG"};
        List<String> LID = Arrays.asList(e);
        Integer[] st = {4,4,3,3,4,3,3,2,3,2};
        List <Integer> HClA = Arrays.asList(st);
        Integer[] z = {4,4,3,3,3,4,3,2,3,2};
        List <Integer> HClB = Arrays.asList(z);
        Integer[] h = {4,4,4,4,3,3,2,3,3,2};
        List <Integer> HClC = Arrays.asList(h);
        int[][] RemDHour = new int [teachercode.size()][5];
        ArrayList<Lessons> lessons = new ArrayList<> ();
        ArrayList<Professor> professors = new ArrayList<> ();
        for (int i=0;i<LID.size();i++){
            lessons.add(new Lessons(LID.get(i),LID.get(i),HClA.get(i),HClB.get(i),HClC.get(i)));
        }
        int[] maxweekly = new int [teachercode.size()];
        for (int i=0;i<teachercode.size();i++){
            Professor temp = new Professor(teachercode.get(i),teachercode.get(i),TeachLesson.get(i),maxdaily.get(i),maxweek.get(i));
            professors.add(temp);
            maxweekly[i] = maxweek.get(i);     // na diagrafei
            for (int j=0;j<lessons.size();j++){
                if (temp.getLessonId().equals(lessons.get(j).getLessonId())){
                    lessons.get(j).addProfessor(temp);
                    break;
                }
            }
        }
        for (int i=0;i<professors.size();i++){
            for (int j=0;j<5;j++){
                RemDHour[i][j] = maxdaily.get(i);
            }
        }
        String[][] program = new String[9][35];
        ArrayList <String> RemLessons = new ArrayList<>();
        Random rand = new Random();
        do{
            goodProgram = true;
            for (int i=0;i<9;i++){
                RemLessons.clear();
                for (int k=0;k<lessons.size();k++){
                    for (int l=0;l<lessons.get(k).getHoursPerClass()[i/3];l++){
                        RemLessons.add(lessons.get(k).getLessonId());
                    }
                }
                for (int k=RemLessons.size()-1;k<35;k++) RemLessons.add("KENO");
                for (int j =0;j<35;j++){
                    program[i][j] = "";
                    added = false;
                    check =0;
                    do {
                        int random = rand.nextInt(RemLessons.size());
                        ArrayList <Integer> Teach = new ArrayList<>();
                        Teach.clear();
                        if (RemLessons.get(random).equals("KENO")){
                            program[i][j] = "KENO";
                            added = true;
                            RemLessons.remove(random);
                        }
                        else {
                            for (int k=0;k<teachercode.size();k++){
                                if (RemLessons.get(random).equals( TeachLesson.get(k) ) ){
                                    boolean add = false;
                                    if (maxweek.get(k)>0 && RemDHour[k][j/7]>0){
                                        add =true;
                                    }
                                    for (int l = 0;l<i;l++){
                                        if (program[l][j].startsWith(teachercode.get(k))){
                                            add = false;
                                        }
                                    }
                                    if (add) Teach.add(k);
                                }
                            }
                            if (!Teach.isEmpty()){
                                ArrayList <Integer> SelTeach= new ArrayList <>();
                                SelTeach.clear();
                                for (int k=0;k<Teach.size();k++){
                                    for (int l=0;l<RemDHour[Teach.get(k)][j/7];l++){
                                        SelTeach.add(Teach.get(k));
                                    }
                                }
                                int rand1 = rand.nextInt(SelTeach.size());
                                program[i][j] = teachercode.get(SelTeach.get(rand1))+"-"+RemLessons.get(random);
                                added = true;
                                RemLessons.remove(random);
                                maxweek.set(SelTeach.get(rand1),maxweek.get(SelTeach.get(rand1))-1);
                                RemDHour[SelTeach.get(rand1)][j/7] -=1;
                            }
                        }
                    check++; 
                    }while (!added && check<10);
                    if (program[i][j].isEmpty()){
                        goodProgram = false;
                        System.out.println("The program is not good");
                        break;
                    }
                }
                if (!goodProgram){
                    break;
                }
            }
            if (!goodProgram){
                for (int i=0;i<teachercode.size();i++){
                   maxweek.set(i, professors.get(i).getMaxWeaklyHour());
                   for (int j=0;j<5;j++){
                        RemDHour [i][j] = professors.get(i).getMaxDailyHour();
                    }
                }
            }
        }while(!goodProgram);
        Node temp = new Node(program,RemDHour,maxweekly,professors,lessons);
        Nodes.add(temp);
        Node temp1;
        for (int i=0;i<1000;i++){
            System.out.println("Score is " + temp.getFitness());
            Nodes.sort(new defaultComparator());
            for (int j=0;j<Nodes.size();j++){
                System.out.println("Score is " + Nodes.get(j).getFitness());
            }
            temp = Nodes.get(0);
            Nodes.clear();
            Nodes.add(temp);
            for (int j=0;j<5;j++){
               temp1 = temp.mutate(temp, 100);
               Nodes.add(temp1);
            }
        }
       System.out.println("Score is " + temp.getFitness());
       /* for (int i =0;i<9;i++){
            System.out.println(i);
            for (int j=0;j<35;j++){
                if ( j%7 == 0) System.out.println();
                System.out.print(program[i][j] + " ");
            }
            System.out.println();
        }*/
        temp1 = temp.mutate(temp, 100);
        /*for (int i =0;i<9;i++){
            System.out.println(i);
            for (int j=0;j<35;j++){
                if ( j%7 == 0) System.out.println();
                System.out.print(temp1.program[i][j] + " ");
            }
            System.out.println();
        }*/
        System.out.println("Score is " + temp1.calculateFitness());
    }
}